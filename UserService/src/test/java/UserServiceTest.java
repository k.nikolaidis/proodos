import org.junit.Test;

import static org.junit.Assert.*;

public class UserServiceTest {

    private String message="";
    @Test
    public void checkIfPasswordsMatch(){
        String textPassword = "123456a", textConPassword = "123456a";


        assertTrue(checkIfPasswordsMatch(textPassword, textConPassword));
    }

    @Test
    public boolean checkIfPasswordIsStrong(String password){
        boolean strongPass = false;
        if(password.length() >= 6) {
            strongPass = true;
        }
        return strongPass;
    }

    @Test
    public void checkIfPasswordIsNull(String password){
        String textPassword = "";


        assertTrue(checkIfPasswordIsNull(textPassword));
    }

    @Test
    public void checkIfUsernameIsNull(){
        String textName = "John";

        assertFalse(checkIfUsernameIsNull(textName));
    }

    @Test
    public void checkIfPasswordsMatch() {
        String textPassword = "123456a", textConPassword = "123456a";


        assertTrue(checkIfPasswordsMatch(textPassword, textConPassword));
    }


    public String checkWarningStatus(String alert){
        message+=alert+"\n";
        return  message;
    }

    public String clearMessage(){
        message="";
        return message;
    }
}
